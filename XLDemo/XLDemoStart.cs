﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.IO;

namespace XLDemo
{
    class XLDemoStart
    {
        static void Main(string[] args)
        {

            int a;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
           
            var filePath = WriteDataMethod();
            ReadData(filePath);
            Console.WriteLine("done");
            Console.WriteLine("hlo");
            Console.WriteLine("3");
            Console.WriteLine("hi");
            Console.WriteLine("2");
        }
        private async static void ReadData(FileInfo filePath)
        {
                var package = new ExcelPackage(filePath);
                await package.LoadAsync(filePath);
                var ws = package.Workbook.Worksheets[0];
                for (int i = 1; i < 5; i++)
                {
                    for(int j = 1; j < 4; j++)
                    {
                        Console.Write(ws.Cells[i, j].Value.ToString() + "\t");
                    }
                    Console.WriteLine();
                }
        }
        private static FileInfo WriteDataMethod()
        {
            Console.WriteLine("Enter File name: ");
            string filename=Console.ReadLine();
            Console.WriteLine("Enter row vale");
            int row =int.Parse( Console.ReadLine());
            Console.WriteLine("Enter column vale");
            int col = int.Parse(Console.ReadLine());
            string filepathstring= @"D:" + filename + ".xlsx";
            var filePath = new FileInfo(fileName: filepathstring);
            var package = new ExcelPackage(filePath);
                var ws = package.Workbook.Worksheets.Add(Name: "MainReport");
                for(int k = 1; k <=col; k++)
                {
                    Console.WriteLine("enter headding cell:");
                    ws.Cells[1, k].Value = Console.ReadLine();
                }
                ws.Row(1).Style.Font.Bold = true;
                for (int i = 2; i <=row+1; i++)
                {
                    Console.WriteLine("enter  Row:"+(i-1)+"Data\n");
                    for (int j = 1; j <=col; j++)
                    {
                        Console.WriteLine("enter cell Data:");
                        ws.Cells[i,j].Value = Console.ReadLine();
                    }    
                  package.Save();
                }
                return filePath;
        }
    }
}
